package dawson;

import static org.junit.Assert.assertTrue;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void echo(){
        App app = new App();
        assertEquals("This test is to see if it echos the number entered as a parameter.", 5, app.echo(5));
    }

    @Test
    public void oneMore(){
        App app = new App();
        assertEquals("This test is to see if it returns the number entered as a parameter plus 1.", 6, app.oneMore(5));
    }
}
